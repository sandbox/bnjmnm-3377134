<?php

namespace Drupal\role_based_form_field_restriction\Form;

use Drupal\Component\Utility\Html;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\Role;
use Drupal\user\RoleInterface;

class FormFieldAccessConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'role_based_form_field_restriction_config_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $wrapper_id = Html::getUniqueId('field-restriction-settings-wrapper');
    $roles = Role::loadMultiple();
    unset($roles[RoleInterface::ANONYMOUS_ID]);
    $roles = array_map(fn(RoleInterface $role) => Html::escape($role->label()), $roles);

    if (empty($form_state->get('conditions'))) {
      $config = $this->config('role_based_form_field_restriction.settings');
      $stored_conditions = $config->get('conditions') ?? [];
      $form_state->set('conditions', $stored_conditions);
    }
    $conditions = $form_state->get('conditions');

    $form['settings'] = [
      '#prefix' => '<div id="' . $wrapper_id . '" class="field-access-settings">',
      '#suffix' => '</div>',
    ];
    $form['#attributes']['class'][] = 'field-access-config-form';

    $condition_form = [
      '#title' => $this->t('Add Condition'),
      '#type' => 'details',
      'condition_config' => $this->conditionConfig('new'),
//      'condition_config' => [
//        '#type' => 'container',
//        '#attributes' => [
//          'class' => ['rb-flex'],
//        ],
//        'form_id_new' => [
//          '#type' => 'textfield',
//          '#title' => $this->t('Form ID'),
//          '#description' => $this->t('Can use wildcard matching')
//        ],
//        'id_type_new' => [
//          '#type' => 'radios',
//          '#title' => $this->t('Form ID Type'),
//          '#options' => $id_options,
//          '#default_value' => 'form_id',
//        ],
//        'roles_new' => [
//          '#type' => 'select',
//          '#title' => $this->t('Roles'),
//          '#options' => $roles,
//          '#multiple' => TRUE,
//        ],
//        'show_hide_new' => [
//          '#type' => 'radios',
//          '#title' => $this->t('Condition Type'),
//          '#options' => $show_hide_options,
//          '#default_value' => 'hide_unless',
//        ],
//      ],
      'add_field' => $this->addFieldFormElements('new', $wrapper_id),
//        [
//        '#prefix' => '<div id="add-field-wrapper-new">',
//        '#suffix' => '</div>',
//        '#type' => 'container',
//        '#markup' => '<h2>' . $this->t('Fields') . '</h2>',
//        '#attributes' => [
//          'class' => ['rb-flex', 'fw-flex'],
//        ],
//        'field_new' => [
//          '#type' => 'textfield',
//          '#description' => $this->t('For fields nested in an array, separate array keys with "]["'),
//        ],
//        'add' => [
//          '#type' => 'submit',
//          '#value' => $this->t('Add'),
//          '#name' => 'add_new_',
//          '#op' => 'add_new_',
//          '#submit' => ['::updateField'],
//          '#ajax' => [
//            'callback' =>  '::updateFieldAjax',
//            'wrapper' => $wrapper_id,
//            'effect' => 'fade',
//          ],
//        ],
//      ],
      'fields_add' => [
        '#prefix' => '<div id="fields-wrapper-add" class="restricted-field-list">',
        '#suffix' => '</div>',
      ],
      'add_condition' => [
        '#type' => 'submit',
        '#value' => $this->t('Add Condition'),
        '#submit' => ['::addCondition'],
        '#op' => 'save_add',
        '#ajax' => [
          'callback' => '::updateFieldAjax',
          'wrapper' => $wrapper_id,
          'effect' => 'fade',
        ],
      ],
    ];

    if (count($conditions) === 0) {
      $condition_form['#open'] = TRUE;
    }

    $form['settings']['add'] = $condition_form;
    $add_list = $form_state->get('field_list') ?? [];

    foreach ($add_list as $key => $item) {
      if (is_array($item)) {
        $form['settings']['add']['fields_add'][] = $this->fieldListEditItem($item, $key, $wrapper_id);
      } else {
        $form['settings']['add']['fields_add']["field_$key"] = $this->fieldListItem($item, $key, $wrapper_id);
      }
    }

    if (count($conditions) > 0) {
      $form['settings']['conditions_header'] = [
        '#markup' => '<h2>' . $this->t('Conditions') . '</h2>',
      ];
    }

    $form['settings']['existing_conditions'] = [];
    foreach ($conditions as $key => $item) {
      $id_type = $item['id_type'] === 'form_id' ? $this->t('Form ID') : $this->t('Base Form ID');
      $form_id = $item['form_id'];
      $roles_string = implode(', ', $item['roles']);
      $condition_description = $item['show_hide'] === 'hide_unless' ? $this->t('Only Visible to These Roles') : $this->t('Hidden From These Roles');
      $form['settings']['existing_conditions']["condition_$key"] = [
        '#type' => 'details',
        '#attributes' => [
          'class' => ['pr'],
        ],
        '#title' => $id_type . ': ' . $item['form_id'] . ', ' . ($item['show_hide'] === 'hide_unless' ? $this->t('visible to') : $this->t('hidden from')) . ' ' . count($item['roles']) . ' ' . $this->t('roles'),
      ];
      if (!empty($item['edit'])) {
        $form['settings']['existing_conditions']["condition_$key"]['#open'] = TRUE;
        $form['settings']['existing_conditions']["condition_$key"]['content'] = [
          'condition_config_' . $key => $this->conditionConfig($key, $item),
          'add_field_' . $key => $this->addFieldFormElements($key, $wrapper_id, TRUE, $key),
          'save_' . $key => [
            '#type' => 'submit',
            '#submit' => ['::updateCondition'],
            '#value' => $this->t('Save Changes'),
            '#name' => "save_condition_$key",
            '#op' => "save_condition_$key",
            '#ajax' => [
              'callback' => '::updateFieldAjax',
              'wrapper' => $wrapper_id,
              'effect' => 'fade',
            ],
            '#attributes' => [
              'class' => ['button--primary'],
            ],
          ],
          'cancel' => [
            '#type' => 'submit',
            '#submit' => ['::updateCondition'],
            '#value' => $this->t('Cancel'),
            '#name' => "cancel_condition_$key",
            '#op' => "cancel_condition_$key",
            '#ajax' => [
              'callback' => '::updateFieldAjax',
              'wrapper' => $wrapper_id,
              'effect' => 'fade',
            ],
            '#attributes' => [
              'class' => ['button--primary'],
            ],
          ],
        ];
      } else {
        $form['settings']['existing_conditions']["condition_$key"]['content'] = [
          'actions' => [
            '#type' => 'container',
            '#attributes' => [
              'class' => ['pa', 'condition-actions'],
            ],
            'edit_' . $key => [
              '#type' => 'submit',
              '#submit' => ['::updateCondition'],
              '#value' => t('Edit'),
              '#name' => "edit_condition_$key",
              '#op' => "edit_condition_$key",
              '#ajax' => [
                'op' => "edit_condition_$key",
                'callback' =>  '::updateFieldAjax',
                'wrapper' => $wrapper_id,
                'effect' => 'fade',
              ],
            ],
            'delete_' . $key => [
              '#type' => 'submit',
              '#value' => t('Delete Condition'),
              '#name' => "remove_condition_$key",
              '#submit' => ['::updateCondition'],
              '#op' => "remove_condition_$key",
              '#ajax' => [
                'callback' =>  '::updateFieldAjax',
                'wrapper' => $wrapper_id,
                'effect' => 'fade',
              ],
            ]
          ],
          'info' => [
            '#markup' => "<b>$id_type:</b> <code>$form_id</code>, <b>$condition_description:</b> <code>$roles_string</code>",
          ],
          'fields' => [
            '#type' => 'container',
            '#attributes' => [
              'class' => 'restricted-field-list'
            ],
            '#markup' => '<hr /><b>' . $this->t('Fields to Restrict') . '<b>',
          ],
        ];
      }
      foreach ($item["field_list"] as $item_key => $field) {
        if (is_array($field)) {
          $form['settings']['existing_conditions']["condition_$key"]['content']['fields'][] = $this->fieldListEditItem($field, $item_key, $wrapper_id, TRUE, $key);
        } else {
          $form['settings']['existing_conditions']["condition_$key"]['content']['fields'][] = $this->fieldListItem($field, $item_key, $wrapper_id, TRUE, $key);
        }
      }
    }

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => t('Save Conditions'),
    ];
    $form['#attached']['library'][] = 'role_based_form_field_restriction/styles';
    return $form;
  }

  public function addCondition($form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $input = $form_state->getUserInput();
    $conditions = $form_state->get('conditions') ?? [];
    $field_list = $form_state->get('field_list');
    $stop = 'here';
    $conditions[] = [
      'form_id' => $values['form_id_new'],
      'id_type' => $values['id_type_new'],
      'roles' => $values['roles_new'],
      'show_hide' => $values['show_hide_new'],
      'field_list' => $field_list,
    ];
    $form_state->set('conditions', $conditions);
    $form_state->set('field_list', []);

    $form["settings"]["add"]["identifier"]["form_id_new"]['#value'] = '';
    $form["settings"]["add"]["identifier"]["id_type_new"]['#value'] = 'form_id';
    $form["settings"]["add"]["identifier"]["roles_new"]['#value'] = [];
    $form["settings"]["add"]["identifier"]["show_hide_new"]['#value'] = 'hide_unless';

    $form_state->setRebuild();

  }

  public function updateCondition($form, FormStateInterface $form_state) {
    $trigger = $form_state->getTriggeringElement();
    $conditions = $form_state->get('conditions') ?? [];
    [$op, $id, $pos, $parent] = explode('_', $trigger['#op']);
    $stop = 'here';
    switch($op) {
      case 'edit':
        if ($id === 'condition') {
          $conditions[(int) $id]['edit'] = TRUE;
        } else {
          $conditions[(int) $parent]['field_list'][(int) $pos] = [
            'edit' => TRUE,
            'item' => $conditions[(int) $parent]['field_list'][(int) $pos],
          ];
        }
        break;
      case 'delete':
        if ($id === 'condition') {
          unset($conditions[(int) $id]);
          $conditions = array_values($conditions);
        } else {
          unset($conditions[(int) $parent]['field_list'][(int) $pos]);
          $conditions[(int) $parent]['field_list'][(int) $pos] = array_values($conditions[(int) $parent]['field_list'][(int) $pos]);
        }
        break;
      case 'cancel': {
        if ($id === 'condition') {
          unset($conditions[(int) $id]['edit']);
        } else {
          unset($conditions[(int) $parent]['field_list'][(int) $pos]);
          $conditions[(int) $parent]['field_list'][(int) $pos] = $conditions[(int) $parent]['field_list'][(int) $pos]['item'];
        }
      }
      case 'save':
        $input = $form_state->getUserInput();
        if ($id === 'condition') {
          $conditions[$pos] = [
            'form_id' => $input["form_id_$pos"],
            'id_type' => $input["id_type_$pos"],
            'roles' => $input["roles_$pos"],
            'show_hide' => $input["show_hide_$pos"],
            'field_list' => $conditions[$pos]['field_list'],

          ];
        } else {

        }
        break;
    }

    $form_state->set('conditions', $conditions);
    $form_state->setRebuild();

  }

  public function updateFieldExistingCondition($form, FormStateInterface $form_state) {
    $trigger = $form_state->getTriggeringElement();
    $values = $form_state->getValues();
    $conditions = $form_state->get('conditions');
    [$op, $id, $pos, $parent] = explode('_', $trigger['#op']);
    switch ($op) {
      case 'add':
        if (!empty($values["field_$pos"])) {
          $conditions[(int) $pos]['field_list'][] = $values["field_$pos"];
        }
        break;
      case 'edit':
        $conditions[(int) $parent]['field_list'][(int) $pos] = [
          'edit' => TRUE,
          'item' => $conditions[(int) $parent]['field_list'][(int) $pos],
        ];
        break;
      case 'save':
        $user_input = $form_state->getUserInput();
        $conditions[(int) $parent]['field_list'][(int) $pos] = $form_state->getUserInput()["field_{$pos}_{$parent}"];
        $stop = 'here';
        break;
      case 'cancel':
        $stop = 'here';
        break;
      case 'delete':
        $stop = 'here';
        break;
    }
    $form_state->set('conditions', $conditions);
    $form_state->setRebuild();
  }

  public function updateField($form, FormStateInterface $form_state) {
    $trigger = $form_state->getTriggeringElement();
    $values = $form_state->getValues();
    [$op, $id, $pos] = explode('_', $trigger['#op']);
    $add_list = $form_state->get('field_list') ?? [];
    switch($op) {
      case 'add':
        if (!empty($values['field_' . $id])) {
          $add_list[] = $values['field_' . $id];
          $form["settings"]["add"]["add_field"]["field_new"]["#value"] = '';
        }
        break;
      case 'edit':
        $stop = 'here';
        $add_list[(int) $pos] = [
          'edit' => TRUE,
          'item' => $add_list[(int) $pos],
        ];
        $stop = 'here';
        break;
      case 'save':
        $add_list[(int) $pos] = $form_state->getUserInput()["field_$pos"];
        break;
      case 'cancel':
        $add_list[(int) $pos] = $add_list[(int) $pos]['item'];
        break;
      case 'delete';
        unset($add_list[(int) $pos]);
        $add_list = array_values($add_list);
        break;
    }
    $form_state->set('field_list', $add_list);
    $form_state->setRebuild();

    $stop = 'hre';
  }

  public function updateFieldAjax(array $form, FormStateInterface $form_state)  {
    $trigger = $form_state->getTriggeringElement();
    [$op, $id] = explode('_', $trigger['#op']);
    $more_trigger = explode('_', $trigger['#op']);

    $form["settings"]["add"]["add_field"]["field_new"]["#value"] = '';

    if ($id === 'new') {
      // If behavior is taking place in the "add" area, keep that
      // area open on rebuild.
      $form["settings"]["add"]['#open'] = TRUE;
    }
    if($id === 'existing') {

      if (isset($more_trigger[2])) {
        $idx = $more_trigger[2];
        if(isset($form['settings']['existing_conditions']['condition_' . $idx])) {
          $form['settings']['existing_conditions']['condition_' . $idx]['#open'] = TRUE;
        }
        // In existing conditions empty "add field" input if condition just
        // added.
        if (isset($form['settings']['existing_conditions']['condition_' . $idx]['content']['add_field_' . $idx]['field_' .$idx]['#value'])) {
          $form['settings']['existing_conditions']['condition_' . $idx]['content']['add_field_' . $idx]['field_' .$idx]['#value'] = '';
        }
      }
    }
      return $form["settings"];
//    return $id === 'new' ? $form["settings"]['add']['fields_add'] : $form['settings']["add_$id"];



  }

  protected function getEditableConfigNames() {
    return ['role_based_form_field_restriction.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory()->getEditable('role_based_form_field_restriction.settings');
    $conditions = $form_state->get('conditions');
    $config->set('conditions', $conditions);
    $config->save();

    $this->messenger()->addMessage($this->t('Configuration has been saved.'));
  }

  public function fieldListItem($item, $key, $wrapper_id, $existing = FALSE, $pos = NULL) {
    $callback = $existing ? '::updateFieldExistingCondition' : '::updateField';
    $stage = $existing ? 'existing' : 'new';
    $loc = !empty($pos) ? $key . '_' . $pos : $key;

    return [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['rb-flex', 'rb-flex-ai-center', 'field-item'],
      ],
      '#markup' => "<b>$item</b>",
      'edit_' . $key => [
        '#type' => 'submit',
        '#submit' => [$callback],
        '#value' => t('Edit'),
        '#name' => "edit_{$stage}_$loc",
        '#op' => "edit_{$stage}_$loc",
        '#ajax' => [
          'callback' =>  '::updateFieldAjax',
          'wrapper' => $wrapper_id,
          'effect' => 'fade',
        ],
      ],
      'delete_' . $key => [
        '#type' => 'submit',
        '#value' => t('Delete'),
        '#name' => "delete_{$stage}_$loc",
        '#submit' => ['::updateField'],
        '#op' => "delete_{$stage}_$loc",
        '#ajax' => [
          'callback' =>  '::updateFieldAjax',
          'wrapper' => $wrapper_id,
          'effect' => 'fade',
        ],
      ]
    ];
  }

  public function fieldListEditItem($item, $key, $wrapper_id, $existing = FALSE, $pos = NULL) {
    $callback = $existing ? '::updateFieldExistingCondition' : '::updateField';
    $loc = !empty($pos) ? $key . '_' . $pos : $key;
    $stage = $existing ? 'existing' : 'new';
    return [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['rb-flex', 'rb-flex-ai-center', 'field-item'],
      ],
      "field_$loc" => [
        '#type' => 'textfield',
        '#value' => $item['item'],
      ],
      'save' => [
        '#type' => 'submit',
        '#submit' => [$callback],
        '#value' => $this->t('Save Changes'),
        '#name' => "save_{$stage}_$loc",
        '#op' => "save_{$stage}_$loc",
        '#ajax' => [
          'callback' => '::updateFieldAjax',
          'wrapper' => $wrapper_id,
          'effect' => 'fade',
        ],
      ],
      'cancel' => [
        '#type' => 'submit',
        '#submit' => [$callback],
        '#value' => $this->t('Cancel'),
        '#name' => "cancel_{$stage}_$loc",
        '#op' => "cancel_{$stage}_$loc",
        '#ajax' => [
          'callback' => '::updateFieldAjax',
          'wrapper' => $wrapper_id,
          'effect' => 'fade',
        ],
      ],
    ];
  }


  public function conditionConfig($key, $values = []) {
    $show_hide_options = [
      'hide_unless' => $this->t('Hide unless matches roles'),
      'show_unless' => $this->t('Show unless matches roles'),
    ];
    $id_options = [
      'form_id' => $this->t('Form ID'),
      'base_form_id' => $this->t('Base Form ID'),
    ];
    $roles = Role::loadMultiple();
    $roles = array_map(fn(RoleInterface $role) => Html::escape($role->label()), $roles);

    return [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['rb-flex'],
      ],
      'form_id_' . $key => [
        '#type' => 'textfield',
        '#title' => $this->t('Form ID'),
        '#description' => $this->t('Can use wildcard matching'),
        '#default_value' => !empty($values['form_id']) ? $values['form_id'] : '',
      ],
      'id_type_' . $key => [
        '#type' => 'radios',
        '#title' => $this->t('Form ID Type'),
        '#options' => $id_options,
//        '#default_value' => $item['id_type'],
//        '#value' => $item['id_type'],
        '#default_value' => !empty($values['id_type']) ? $values['id_type'] : 'form_id',

      ],
      'roles_' . $key => [
        '#type' => 'select',
        '#title' => $this->t('Roles'),
        '#options' => $roles,
        '#multiple' => TRUE,
//        '#value' => $item['roles'],
//        '#default_value' => $item['roles'],
        '#default_value' => !empty($values['roles']) ? $values['roles'] : [],

      ],
      'show_hide_' . $key => [
        '#type' => 'radios',
        '#title' => $this->t('Condition Type'),
        '#options' => $show_hide_options,
//        '#value' => $item['show_hide'],
//        '#default_value' => $item['show_hide'],
        '#default_value' => !empty($values['show_hide']) ? $values['show_hide'] : 'hide_unless',
      ],
    ];
  }

  public function addFieldFormElements($key, $wrapper_id, $existing = FALSE, $pos = 0) {
    $callback = $existing ? '::updateFieldExistingCondition' : '::updateField';
    $stage = $existing ? 'existing' : 'new';
    $loc = !empty($pos) ? $key . '_' . $pos : $key;

    return [
      '#prefix' => '<div>',
      '#suffix' => '</div>',
      '#type' => 'container',
      '#markup' => '<h2>' . $this->t('Fields') . '</h2>',
      '#attributes' => [
        'class' => ['rb-flex', 'fw-flex'],
      ],
      "field_" . ($existing ? $loc : $key) => [
        '#type' => 'textfield',
        '#description' => $this->t('For fields nested in an array, separate array keys with "]["'),
      ],
      'add_' . $key => [
        '#type' => 'submit',
        '#value' => $this->t('Add'),
        '#name' => "add_{$stage}_$loc",
        '#op' => "add_{$stage}_$loc",
        '#submit' => [$callback],
        '#ajax' => [
          'callback' =>  '::updateFieldAjax',
          'wrapper' => $wrapper_id,
          'effect' => 'fade',
        ],
      ],
    ];
  }
}
